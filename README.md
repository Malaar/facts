# Facts

Project was created with paradigm of Protocol Oriented Programming and SOLID principles.
Project demonstrates work with MVVM pattern, multithreading, dependency injections, networking, custom layouts and other things.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for overview and testing purposes.

### Prerequisites

* Mac OS X 10.13+, Xcode 9.3+

### Installing

1. Clone Mint repository

			git clone git@bitbucket.org:Malaar/facts.git

2. Done.

## Dependency:

In this project I use Mint framework.
Mint framework - my framework to work with remote resources (in-memory cache)


## Author

* **[Andrew Korshilovskiy](http://www.linkedin.com/in/korshilovskiy)** - development

## License

(c) All rights reserved.
This project is licensed under the **Proprietary Software License** - see the [LICENSE](http://www.binpress.com/license/view/l/358023be402acff778a934083b76b86f) url for details
