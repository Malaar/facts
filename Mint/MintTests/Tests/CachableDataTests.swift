//
//  CachableDataTests.swift
//  MintTests
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import Mint

class CachableDataTests: XCTestCase {
    
    func testMemorySize_afterDataCreation() {
        let data1 = Data(count: 1024)
        let cachableData = CachableData(identifier: "test", data: data1)
        XCTAssertTrue(cachableData.memorySize == data1.count, "Memory size should be equal")
    }

    func testMemorySize_afterDataChenging() {
        let data1 = Data(count: 1024)
        let cachableData = CachableData(identifier: "test", data: data1)
        let data2 = Data(count: 2048)
        cachableData.data = data2
        XCTAssertFalse(cachableData.memorySize == data1.count, "Memory size should changed")
    }
}
