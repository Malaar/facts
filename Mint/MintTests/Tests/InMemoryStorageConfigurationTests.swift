//
//  InMemoryStorageConfigurationTests.swift
//  MintTests
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import Mint

class InMemoryStorageConfigurationTests: XCTestCase {
    
    func testMaxCapacity_Changes() {
        let maxCapacity = 10
        let configuration = InMemoryStorageConfiguration(maxCapacity: maxCapacity, maxMemoryUsage: 0)
        XCTAssertTrue(configuration.maxCapacity == maxCapacity, "maxCapacity should be the same")
    }

    func testMaxMemoryUsage_Changes() {
        let maxMemoryUsage = 1024
        let configuration = InMemoryStorageConfiguration(maxCapacity: 10, maxMemoryUsage: maxMemoryUsage)
        XCTAssertTrue(configuration.maxMemoryUsage == maxMemoryUsage, "maxMemoryUsage should be the same")
    }
}
