//
//  MocResourceGateway.swift
//  MintTests
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import Mint

class MocResourceGateway: Gatewayable {
    
    var simulateServerError: Bool = false
    var simulateNoData: Bool = false
    
    private func generateResponse() {
        
    }

    func resource(withURL url: URL, callback: @escaping GatewayCallback) -> GatewayToken? {
        var statusCode = 200
        if simulateServerError {
            statusCode = 404
            let error = NSError(domain: "ServiceDomain", code: statusCode, userInfo: nil)
            callback(nil, statusCode, error)
            return nil
        }
        
        if simulateNoData {
            callback(nil, statusCode, nil)
            return nil
        }
        
        let data = Data(count: 1024)
        let resource = CachableData(identifier: url.absoluteString, data: data)
        callback(resource, statusCode, nil)
        
        return nil
    }

}
