//
//  ResourceCache.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ResourceCache

open class ResourceCache {
    
    public typealias ResourceCacheCallback = (ResourceCachable?, Error?) -> Void
    
    public struct Defaults {
        public static let maxConcurrentOperationCount = 0
        fileprivate static let queueName = "com.malaar.mint.cacheQueue"
    }
    
    public let gateway: Gatewayable
    public let storage: Storageable
    public let cacheQueue: OperationQueue
    
    public init(gateway: Gatewayable, storage: Storageable, maxConcurrentOperationCount: Int = Defaults.maxConcurrentOperationCount) {
        self.gateway = gateway
        self.storage = storage
        self.cacheQueue = OperationQueue()
        cacheQueue.maxConcurrentOperationCount = maxConcurrentOperationCount
        cacheQueue.name = Defaults.queueName
    }
    
    open func resource(forURL url: URL, callback: @escaping ResourceCacheCallback) -> ResourceToken? {
        return resource(forURL: url, gateway: gateway, saveOperation: SaveResourceOperation(storage: storage), callback: callback)
    }
    
    func resource<SaveType: SaveOperationType>(forURL url: URL,
                                               gateway: Gatewayable,
                                               saveOperation: SaveType,
                                               callback: @escaping ResourceCacheCallback) -> ResourceToken? {
        return fetchResource(forURL: url, gateway: gateway, prepareOperations: { () -> [ResourceOperationType] in
            saveOperation.completionBlock = { [unowned saveOperation] in
                    if !saveOperation.isCancelled {
                        callback(saveOperation.resource, saveOperation.error)
                    }
                }
                return [saveOperation]

            }, callback: callback)
    }
    
    func resource<ProcessType: ProcessOperationType, SaveType: SaveOperationType>(forURL url: URL,
                   gateway: Gatewayable,
                   processOperations: [ProcessType],
                   saveOperation: SaveType,
                   callback: @escaping ResourceCacheCallback) -> ResourceToken? {
        return fetchResource(forURL: url, gateway: gateway, prepareOperations: { () -> [ResourceOperationType] in
            saveOperation.completionBlock = { [unowned saveOperation] in
                if !saveOperation.isCancelled {
                    callback(saveOperation.resource, saveOperation.error)
                }
            }
            let operations = prepareOperations(processOperations: processOperations, saveOperation: saveOperation)
            return operations
        }, callback: callback)
    }
    
    private func fetchResource(forURL url: URL,
                               gateway: Gatewayable,
                               prepareOperations: () -> [ResourceOperationType],
                               callback: @escaping ResourceCacheCallback) -> ResourceToken? {
        if storage.hasResource(forKey: url.absoluteString) {
            storage.resource(forKey: url.absoluteString) { resource in
                callback(resource, nil)
            }
            return nil
        }
        
        let operations = prepareOperations()
        
        let gatewayToken = gateway.resource(withURL: url) { (resource, statusCode, error) in
            if let error = error as NSError?, error.code == NSURLErrorCancelled {
                print("GATEWAY DOWNLOAD CANCELED")
                return
            }
            if let error = error {
                print("GATEWAY ERROR: " + error.localizedDescription)
            }
//            callback(resource, error)

            if var operation = operations.first {
                operation.resource = resource
                operation.error = error
            }
            self.cacheQueue.addOperations(operations, waitUntilFinished: false)
        }
        
        return ResourceToken(resourceUrl: url, gatewayToken: gatewayToken, operations: operations)
//        let operations = [Operation]()
//        return ResourceToken(resourceUrl: url, gatewayToken: gatewayToken, operations: operations)
    }
    
    private func prepareOperations<ProcessType: ProcessOperationType, SaveType: SaveOperationType>(processOperations: [ProcessType], saveOperation: SaveType) -> [ResourceOperationType] {
        var operations = [ResourceOperationType]()
        
        var previousOperation: Operation? = nil
        for processOperation in processOperations {
            if let previousOperation = previousOperation {
                processOperation.addDependency(previousOperation)
            }
            previousOperation = processOperation
        }
        operations.append(contentsOf: processOperations)
        
        if let previousOperation = previousOperation {
            saveOperation.addDependency(previousOperation)
        }
        operations.append(saveOperation)
        
        return operations
    }
}
