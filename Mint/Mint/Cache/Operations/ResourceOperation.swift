//
//  ResourceOperation.swift
//  Mint
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ResourceOperation

open class ResourceOperation: Operation, ResourceOperationable {
    
    open var resource: ResourceCachable? {
        get {
            return internalResource != nil ? internalResource : resourceFromDependentOperation()
        }
        set {
            internalResource = newValue
        }
    }
    
    open var error: Error? {
        get {
            return internalError != nil ? internalError : errorFromDependentOperation()
        }
        set {
            internalError = newValue
        }
    }
    
    private var internalResource: ResourceCachable?
    private var internalError: Error?
}
