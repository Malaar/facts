//
//  ResourceToken.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ResourceToken

public class ResourceToken {
    
    public let resourceUrl: URL
    public private (set) var isCanceled: Bool = false
    private weak var gatewayToken: GatewayToken?
    private let operations: [Operation]
    
    init(resourceUrl: URL, gatewayToken: GatewayToken?, operations: [Operation]) {
        self.resourceUrl = resourceUrl
        self.gatewayToken = gatewayToken
        self.operations = operations
    }
    
    public func cancel() {
        guard !isCanceled else { return }
        isCanceled = true
        gatewayToken?.cancel()
        operations.forEach { $0.cancel() }
    }
}
