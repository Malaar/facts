//
//  ResourceDataCachable.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - CachableData

open class CachableData: ResourceCachable {
    
    open private(set) var identifier: String
    open private(set) var memorySize: Int
    open var data: Data? {
        didSet {
            self.memorySize = data != nil ? data!.count : 0
        }
    }
    
    public init(identifier: String, data: Data) {
        self.identifier = identifier
        self.data = data
        self.memorySize = data.count
    }
}
