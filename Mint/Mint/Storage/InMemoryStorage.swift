//
//  InMemoryStorage.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - InMemoryStorage

public class InMemoryStorage: Storageable {
    
    public let configuration: InMemoryStorageConfiguration
    public private(set) var currentMemoryUsage: Int
    public var currentCapacity: Int {
        return cache.count
    }
    private var cache: Dictionary<String, ResourceCachable>
    private var cachedItems: [ResourceCachable]
    private let storageQueue: DispatchQueue
    
    
    //MARK: -
    
    public init(configuration: InMemoryStorageConfiguration) {
        self.configuration = configuration
        self.currentMemoryUsage = 0
        
        self.cache = Dictionary<String, ResourceCachable>()
        self.cachedItems = [ResourceCachable]()
        self.storageQueue = DispatchQueue(label: InMemoryStorageConfiguration.Defaults.queueName)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveMemoryWarning(notification:)),
                                               name: Notification.Name.UIApplicationDidReceiveMemoryWarning,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //MARK: - Resource management
    
    public func hasResource(forKey identifier: String) -> Bool {
        return storageQueue.sync { [unowned self] in self.cache[identifier] != nil }
    }
    
    public func resource(forKey identifier: String, callback: @escaping CallbackWithOptionalCachable) {
        storageQueue.async { [unowned self] in
            callback(self.cache[identifier])
        }
    }

    public func resourceSync(forKey identifier: String) -> ResourceCachable? {
        return storageQueue.sync { [unowned self] in self.cache[identifier] }
    }
    
    public func setResource(_ resource: ResourceCachable, forKey identifier: String, callback: CallbackEmpty?) {
        if resource.memorySize == 0 {   //don't need to store resource with empty size
            callback?()
            return
        }
        storageQueue.async { [unowned self] in
            self.evictItems(forNewSize: resource.memorySize, newCount: 1)
            self.addResource(resource, forKey: identifier)
            callback?()
        }
    }
    
    public func setResourceSync(_ resource: ResourceCachable, forKey identifier: String) {
        if resource.memorySize == 0 {   //don't need to store resource with empty size
            return
        }
        storageQueue.sync {
            self.evictItems(forNewSize: resource.memorySize, newCount: 1)
            self.addResource(resource, forKey: identifier)
        }
    }
    
    public func removeAllResources() {
        storageQueue.sync { [unowned self] in
            self.cache.removeAll()
            self.cachedItems.removeAll()
            self.currentMemoryUsage = 0
        }
    }
    
    
    //MARK: - Private methods
    
    private func addResource(_ resource: ResourceCachable, forKey identifier: String) {
        if let oldResource = cache[identifier] {
            cachedItems.remove(at: cachedItems.index(where: { $0.identifier == identifier })!)
            currentMemoryUsage -= oldResource.memorySize
        }
        cache[identifier] = resource
        cachedItems.append(resource)
        currentMemoryUsage += resource.memorySize
    }
    
    private func removeResource(forKey identifier: String, cachedItemIndex: Int? = nil) {
        if let resource = cache[identifier] {
            cache.removeValue(forKey: identifier)
            if let index = cachedItemIndex {
                cachedItems.remove(at: index)
            } else {
                cachedItems.remove(at: cachedItems.index(where: { $0.identifier == identifier })!)
            }
            currentMemoryUsage -= resource.memorySize
        }
    }
    
    private func evictItems(forNewSize newSize: Int, newCount: Int) {
        if configuration.maxMemoryUsage != 0 && currentMemoryUsage + newSize <= configuration.maxMemoryUsage { return }
        if cache.count + newCount <= configuration.maxCapacity { return }
        
        let oldestItem = cachedItems.first
        if let item = oldestItem {
            removeResource(forKey: item.identifier, cachedItemIndex: 0)
            evictItems(forNewSize: newSize, newCount: newCount)
        }
    }
    
    
    //MARK: - Memory warning processing
    
    @objc private func didReceiveMemoryWarning(notification: Notification) {
        removeAllResources()
    }
}
