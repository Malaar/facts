//
//  ResourceGateway.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ResourceGateway

open class ResourceGateway: Gatewayable {
    
    public let urlSession: URLSession
    
    public init(sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.ephemeral) {
        urlSession = URLSession(configuration: sessionConfiguration)
    }
    
    open func resource(withURL url: URL, callback: @escaping GatewayCallback) -> GatewayToken? {
        let request = URLRequest(url: url)
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            print("DOWNLOADING COMPLETE")
            NetworkActivityIndicatorManager.instance.networkActivityIndicatorVisible(false)
            var statusCode = 0
            if let response = response as? HTTPURLResponse {
                statusCode = response.statusCode
            }
            let resource = data != nil ? CachableData(identifier: url.absoluteString, data: data!) : nil
            callback(resource, statusCode, error)
        }
        dataTask.resume()
        print("DOWNLOADING START")
        NetworkActivityIndicatorManager.instance.networkActivityIndicatorVisible(true)
        return dataTask
    }
}
