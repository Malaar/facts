//
//  Gatewayable.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


public typealias GatewayToken = URLSessionDataTask
public typealias ResponseStatusCode = Int
public typealias GatewayCallback = (ResourceCachable?, ResponseStatusCode, Error?) -> Void

//MARK: - Gatewayable protocol

public protocol Gatewayable {
    
    func resource(withURL url: URL, callback: @escaping GatewayCallback) -> GatewayToken?
}
