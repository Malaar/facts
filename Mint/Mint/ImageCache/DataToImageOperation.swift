//
//  DataToImageOperation.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - DataToImageOperation

final public class DataToImageOperation: ProcessResourceOperation, DataToImageOperationable {
    
    public var resultImage: ResourceImageCachable? {
        didSet {
            self.resultResource = resultImage
        }
    }
    
    public override func main() {
        guard let resource = resource, let data = resource.data else { return }
        resultImage = CachableImage(identifier: resource.identifier, data: data)
    }
}
