//
//  ProcessImageOperation.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ProcessImageOperation

open class ProcessImageOperation: ProcessResourceOperation {
    
    public private(set) var image: ResourceImageCachable?
    public var resultImage: ResourceImageCachable? {
        didSet {
            self.resultResource = resultImage
        }
    }
}

