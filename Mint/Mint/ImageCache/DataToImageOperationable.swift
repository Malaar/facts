//
//  DataToImageOperationable.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - DataToImageOperationable protocol

public protocol DataToImageOperationable: ProcessResourceOperationable {
    
    var resultImage: ResourceImageCachable? { get }
}
