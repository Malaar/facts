//
//  CachableImage.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - CachableImage

open class CachableImage: ResourceImageCachable {
    
    open private(set) var identifier: String
    open private(set) var memorySize: Int
    open var image: UIImage?
    public var data: Data? = nil
    
    public init(identifier: String, data: Data) {
        self.identifier = identifier
        self.image = UIImage(data: data)
        self.memorySize = image != nil ? data.count : 0
    }
}
