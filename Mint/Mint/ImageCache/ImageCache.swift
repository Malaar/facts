//
//  ImageCache.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ImageCache

open class ImageCache: ResourceCache {
    
    public typealias ImageCacheCallback = (ResourceImageCachable?, Error?) -> Void
    
//    open override func resource(forURL url: URL,
//                                processOperations: [Operation & ProcessResourceOperationable]?,
//                                callback: @escaping ResourceCache.ResourceCacheCallback) -> ResourceToken? {
//        var operations: [Operation & ProcessResourceOperationable] = [DataToImageOperation()]
//        if let processOperations = processOperations {
//            operations.append(contentsOf: processOperations)
//        }
//        return super.resource(forURL: url, processOperations: operations, callback: callback)
//    }
    
//    open func resource(forURL url: URL,
//                       processOperations: [ProcessImageOperation]?,
//                       callback: @escaping ImageCacheCallback) -> ResourceToken? {
//        return self.resource(forURL: url, processOperations: processOperations, callback: callback)
//    }
}
