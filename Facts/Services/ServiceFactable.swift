//
//  ServiceFactable.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ServiceFactable protocol

protocol ServiceFactable {
    
    typealias ServiceFactableCallback = ([Factable]?, Error?) -> Void
    
    func facts(offset: Int, limit: Int, callback: @escaping ServiceFactableCallback)
}
