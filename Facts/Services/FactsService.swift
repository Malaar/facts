//
//  FactsService.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Constants

fileprivate struct Constants {
//    static let factsURL = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")!
    static let factsURL = URL(string: "https://www.pastebin.com/raw/QkwsMMn1")!
}


//MARK: - FactsService

class FactsService: ServiceFactable {
    
    private let urlSession: URLSession
    
    init(configuration: URLSessionConfiguration = URLSessionConfiguration.default) {
        self.urlSession = URLSession(configuration: configuration)
    }
    
    func facts(offset: Int, limit: Int, callback: @escaping ([Factable]?, Error?) -> Void) {
        var request = URLRequest(url: Constants.factsURL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            if error != nil {
                callback(nil, error)
                return
            }
            guard let data = data else {
                callback(nil, error)
                return
            }
            do {
                let response = try JSONDecoder().decode(FactResponse.self, from: data)
                let facts = response.rows
                callback(facts, error)
            } catch {
                callback(nil, error)
            }
        }
        task.resume()
    }
}
