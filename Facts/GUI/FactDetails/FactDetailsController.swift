//
//  FactDetailsController.swift
//  Facts
//
//  Created by Malaar on 6/11/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - Constants

fileprivate struct Constants {
    
    static let defaultConstraintConstant: CGFloat = 8.0
    static let headerViewLandscapeConstraintConstant: CGFloat = 18.0
    static let headerViewPortraitConstraintConstant: CGFloat = 0.0
    static let landscapeHeaderContentRatio: CGFloat = 0.33
}


//MARK: - FactDetailsController

class FactDetailsController: UIViewController {
    
    var viewModel: FactDetailsViewModelable!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet private weak var factImageView: UIImageView!
    @IBOutlet private weak var factDescriptionLabel: UILabel!
    
    @IBOutlet weak var contentViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var factImageViewAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet var portraitOnlyConstraints: [NSLayoutConstraint]!
    @IBOutlet var headerPortraitConstraints: [NSLayoutConstraint]!

    lazy var landscapeOnlyConstraints: [NSLayoutConstraint] = {
        let c1 = NSLayoutConstraint(item: self.headerView, attribute: .trailing, relatedBy: .equal, toItem: self.bodyView, attribute: .leading, multiplier: 1.0, constant: -Constants.defaultConstraintConstant)
        let c2 = NSLayoutConstraint(item: self.headerView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: self.contentView, attribute: .bottom, multiplier: 1.0, constant: -Constants.defaultConstraintConstant)
        let c3 = NSLayoutConstraint(item: self.bodyView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1.0, constant: Constants.defaultConstraintConstant)
        let c4 = NSLayoutConstraint(item: self.headerView, attribute: .width, relatedBy: .equal, toItem: self.bodyView, attribute: .width, multiplier: Constants.landscapeHeaderContentRatio, constant: 0)
        return [c1, c2, c3, c4]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = viewModel.factTitle
        self.factDescriptionLabel.text = viewModel.factDesctiption
        setupViewModelCallbacks()
        setupConstraintsForOrientation()

        viewModel.loadImage()
    }
    
    func setupViewModelCallbacks() {
        viewModel.loadImageCallback = { [weak self] image in
            guard let strongSelf = self else { return }
            strongSelf.factImageView.image = image
            let aspectRatio = image.size.width / image.size.height
            let aspectRatioConstraint = NSLayoutConstraint(item: strongSelf.factImageView, attribute: .width, relatedBy: .equal, toItem: strongSelf.factImageView, attribute: .height, multiplier: aspectRatio, constant: 0)
            self?.factImageViewAspectRatioConstraint.isActive = false
            self?.factImageViewAspectRatioConstraint = aspectRatioConstraint
            self?.factImageViewAspectRatioConstraint.isActive = true
            self?.factImageView.setNeedsUpdateConstraints()
        }
    }
}


//MARK: - Layout & constraints

extension FactDetailsController {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { [unowned self] (context) in
            self.setupConstraintsForOrientation()
            }, completion: nil)
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func setupConstraintsForOrientation() {
        if UIScreen.main.isLandscapeOrientation {
            self.portraitOnlyConstraints.forEach { $0.isActive = false }
            self.landscapeOnlyConstraints.forEach { $0.isActive = true }
            self.headerPortraitConstraints.forEach { $0.constant = Constants.headerViewLandscapeConstraintConstant }
        } else {
            self.landscapeOnlyConstraints.forEach { $0.isActive = false }
            self.portraitOnlyConstraints.forEach { $0.isActive = true }
            self.headerPortraitConstraints.forEach { $0.constant = Constants.headerViewPortraitConstraintConstant }
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let scrollView = self.contentView.superview as! UIScrollView
        let insets = contentInsetWithSafeArea()
        scrollView.contentInset = insets
        contentViewWidthConstraint.constant = -(insets.left + insets.right)
    }

    func contentInsetWithSafeArea() -> UIEdgeInsets {
        let insets: UIEdgeInsets
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            insets = UIEdgeInsets(top: 0, left: window.safeAreaInsets.left, bottom: 0, right: window.safeAreaInsets.right)
        } else {
            insets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        return insets
    }
}
