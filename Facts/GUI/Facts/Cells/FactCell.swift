//
//  FactCell.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - FactCell

class FactCell: UICollectionViewCell {
    
    struct Constants {
        
        static let reuseIdentifier = "FactCell"
        fileprivate static let thumbnailImageName = "ImageThumbnail"
        fileprivate static let titleFont = UIFont(name: "HelveticaNeue", size: 18)!
        fileprivate static let imageMargin: CGFloat = 13
        
        fileprivate static let thumbnailImageHeight: CGFloat = {
            return UIImage(named: Constants.thumbnailImageName)?.size.height ?? 0
        }()

    }
    
    var viewModel: FactCellViewModelable?
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    
    func configure(viewModel: FactCellViewModelable?) {
        self.viewModel = viewModel
        viewModel?.addObserverOnImageDidLoad(observer: self,
                                             selector: #selector(remoteImageDidLoad(notification:)),
                                             name: RemoteImageDisposer.ImageDidLoadNotification,
                                             object: nil)
        self.titleLabel.text = viewModel?.title
        if let image = viewModel?.image {
            self.imageView.image = image
        } else {
            self.imageView.image = UIImage(named: Constants.thumbnailImageName)
            viewModel?.loadImage()
        }
    }
    
    func didEndDisplaying() {
        viewModel?.cancelLoadingImage()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        viewModel?.removeObserverOnImageDidLoad(observer: self)
    }
}


//MARK: - Image loading

extension FactCell {
    
    @objc private func remoteImageDidLoad(notification: Notification) {
        let image = notification.userInfo?[RemoteImageDisposer.ImageDidLoadNotificationImageKey] as? UIImage
        self.imageView.image = image ?? UIImage(named: Constants.thumbnailImageName)
    }
}


//MARK: - Cell height

extension FactCell {

    static func heightForFactCellViewModel(_ cellViewModel: FactCellViewModelable?, restrictingWidth: CGFloat, imageSize: CGSize? = nil) -> CGFloat {
        var titleHeight: CGFloat = 0.0
        if let title = cellViewModel?.title {
            let title = title as NSString
            let constraintSize = CGSize(width: restrictingWidth, height: .greatestFiniteMagnitude)
            let rect = title.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin, attributes: [.font: Constants.titleFont], context: nil)
            titleHeight = ceil(rect.height)
        }
        
        var imageViewSize = CGSize()
        imageViewSize.width = restrictingWidth - 2 * Constants.imageMargin
        
        if let imageSize = imageSize {
            let aspectRatio = imageSize.height / imageSize.width
            imageViewSize.height = imageViewSize.width * aspectRatio
        } else {
            imageViewSize.height = Constants.thumbnailImageHeight
        }
        
        let height = 3 * Constants.imageMargin + imageViewSize.height + titleHeight
        return height
    }
}
