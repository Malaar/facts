//
//  FactsController.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import Mint


//MARK: - Constants

fileprivate struct Constants {
    
    static let refreshControlColor = UIColor(hex: "D5171A")
    static let showFactDetailsSegueIdentifier = "showFactDetails"

    struct CollectionLayout {
        
        struct Phone {
            static let itemMargin: CGFloat = 10.0
            static let columnsNumberPortrait = 1
            static let columnsNumberLandscape = 2
        }
        
        struct Pad {
            static let itemMargin: CGFloat = 20.0
            static let columnsNumberPortrait = 2
            static let columnsNumberLandscape = 3
        }
        
        static func itemMargin(traitCollection: UITraitCollection) -> CGFloat {
            return traitCollection.userInterfaceIdiom == .phone ? Phone.itemMargin : Pad.itemMargin
        }
        
        static func columnsNumber(traitCollection: UITraitCollection) -> Int {
            if traitCollection.userInterfaceIdiom == .phone {
                return UIScreen.main.isPortraitOrientation ? Phone.columnsNumberPortrait : Phone.columnsNumberLandscape
            } else {
                if traitCollection.horizontalSizeClass == .compact {
                    return Pad.columnsNumberPortrait
                } else {
                    return UIScreen.main.isPortraitOrientation ? Pad.columnsNumberPortrait : Pad.columnsNumberLandscape
                }
            }
        }
    }
}


//MARK: - FactsController

class FactsController: UICollectionViewController {

    private var viewModel: FactsViewModelCollectable!
    private var refreshControl: UIRefreshControl!


    //MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.edgesForExtendedLayout = []

        self.viewModel = createViewModel()
        setupViewModelCallbacks()
        setupCollection()
        setupRefreshControl()
        fetchData(reload: true)
    }

    
    // MARK: Configuration
    
    func createViewModel() -> FactsViewModelCollectable? {
        //TODO: Add Swinject to resolve dependency injections
        let service = FactsService()
        let gateway = ResourceGateway()
        let storage = InMemoryStorage(configuration: InMemoryStorageConfiguration(maxCapacity: 100, maxMemoryUsage: 0))
        let cache = ResourceCache(gateway: gateway, storage: storage, maxConcurrentOperationCount: 10)
        let viewModel = FactsViewModel(factsService: service, resourceCache: cache)
        return viewModel
    }

    func setupViewModelCallbacks() {
        viewModel.reloadCallback = { [weak self] (reload, insertIndexPaths, error) in
            if self?.refreshControl.isRefreshing ?? false {
                self?.refreshControl.endRefreshing()
            }
            if error != nil {
//                HUD.flash(.error, onView: self?.view, delay: 1.0, completion: nil)
            } else {
                if reload {
//                    HUD.hide(animated: true)
                    self?.collectionView?.reloadData()
                } else if let insertIndexPaths = insertIndexPaths {
                    self?.collectionView?.insertItems(at: insertIndexPaths)
                }
            }
        }
        viewModel.loadImageCallback = { [weak self] image in
            UIView.animate(withDuration: 0.3, animations: {
                self?.collectionView?.collectionViewLayout.invalidateLayout()
            })
        }
    }

    func setupCollection() {
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.backgroundColor = .groupTableViewBackground
        let margin = Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
        self.collectionView?.contentInset = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
        if let collectionLayout = self.collectionView?.collectionViewLayout as? CollectionLayout {
            collectionLayout.delegate = self
        }
    }
    
    func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = Constants.refreshControlColor
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.collectionView?.refreshControl = refreshControl
        } else {
            self.collectionView?.addSubview(refreshControl)
        }
    }

    
    //MARK: Data management
    
    @objc private func reloadData() {
        fetchData(reload: true)
    }
    
    private func fetchData(reload: Bool) {
        if reload {
//            HUD.show(.progress, onView: self.view)
        }
        viewModel?.fetchData(reload: reload)
    }

}


//MARK: - UICollectionViewDataSource

extension FactsController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.itemsCount()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FactCell.Constants.reuseIdentifier, for: indexPath) as! FactCell
        let cellViewModel = viewModel.item(at: indexPath)
        cell.configure(viewModel: cellViewModel)
        return cell
    }
}


//MARK: - UICollectionViewDelegate

extension FactsController {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == viewModel.itemsCount() - 1 {
            viewModel.fetchData(reload: false)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? FactCell {
            cell.didEndDisplaying()
        }
    }
}


//MARK: - CollectionLayoutDelegate

extension FactsController: CollectionLayoutDelegate {

    func collectionView(numberOfCollumnsForCollectionView: UICollectionView) -> Int {
        return Constants.CollectionLayout.columnsNumber(traitCollection: self.traitCollection)
    }
    
    func collectionView(itemMargin: UICollectionView) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }

    func collectionView(_ collectionView: UICollectionView, heightForItemAtIndexPath indexPath: IndexPath, itemWidth: CGFloat) -> CGFloat {
        let cellViewModel = viewModel.item(at: indexPath)
        return FactCell.heightForFactCellViewModel(cellViewModel, restrictingWidth: itemWidth, imageSize: cellViewModel?.imageSize)
    }
}


//MARK: - Navigation

extension FactsController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.showFactDetailsSegueIdentifier {
            let destinationController = segue.destination as! FactDetailsController
            if let factCell = sender as? FactCell, let viewModel = factCell.viewModel?.factDetailsViewModel {
                destinationController.viewModel = viewModel
            }
        }
    }
}


//MARK: - Layout

extension FactsController {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let insets = contentInsetWithSafeArea()
        collectionView?.contentInset = insets
    }
    
    func contentInsetWithSafeArea() -> UIEdgeInsets {
        let insets: UIEdgeInsets
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            insets = UIEdgeInsets(top: 0, left: window.safeAreaInsets.left, bottom: 0, right: window.safeAreaInsets.right)
        } else {
            insets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        return insets
    }
}
