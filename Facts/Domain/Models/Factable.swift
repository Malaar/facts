//
//  Factable.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Factable protocol

protocol Factable {

    var title: String? { get }
    var description: String? { get }
    var imageUrl: URL? { get }

    func isEqual(to other: Factable) -> Bool
}
