//
//  Fact.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Fact

class Fact: Factable, Codable {
    
    let title: String?
    let description: String?
    let imageUrl: URL?
    
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case imageUrl = "imageHref"
    }
    
    init(title: String, description: String, imageUrl: URL) {
        self.title = title
        self.description = description
        self.imageUrl = imageUrl
    }
    
    func isEqual(to other: Factable) -> Bool {
        guard let other = other as? Fact else { return false }
        return self == other
    }
}


//MARK: - Equatable

extension Fact: Equatable {
    
    static func == (lhs: Fact, rhs: Fact) -> Bool {
        return lhs.title == rhs.title && lhs.imageUrl == rhs.imageUrl && lhs.description == rhs.description
    }
}
