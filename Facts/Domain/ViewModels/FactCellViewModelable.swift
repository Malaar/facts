//
//  FactCellViewModelable.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - FactCellViewModelable

protocol  FactCellViewModelable {
    
    var title: String? { get }
    var image: UIImage? { get }
    var imageSize: CGSize? { get }
    
    func loadImage()
    func cancelLoadingImage()
    
    func addObserverOnImageDidLoad(observer: Any, selector: Selector, name: NSNotification.Name?, object: Any?)
    func removeObserverOnImageDidLoad(observer: Any)
    
    var factDetailsViewModel: FactDetailsViewModelable { get }
}
