//
//  FactsViewModel.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import Mint


//MARK: - FactsViewModel

class FactsViewModel: FactsViewModelCollectable {
    
    var reloadCallback: ReloadCallback?
    var loadImageCallback: LoadImageCallback?
    var pagingCursor: PagingCursor
    var pagesFinished = false
    private var cellViewModels = [FactCellViewModelable]()
    private let factsService: ServiceFactable
    private let resourceCache: ResourceCache
    
    init(factsService: ServiceFactable, resourceCache: ResourceCache) {
        self.factsService = factsService
        self.resourceCache = resourceCache
        pagingCursor = PagingCursor()
    }
}


//MARK: - FactsViewModelCollectable

extension FactsViewModel {
    
    func itemsCount() -> Int {
        return cellViewModels.count
    }
    
    func item(at indexPath: IndexPath) -> FactCellViewModelable? {
        return indexPath.row < cellViewModels.count ? cellViewModels[indexPath.row] : nil
    }
    
    func fetchData(reload: Bool) -> Void {
        if !reload && pagesFinished { return }
        
        if reload {
            resetPage()
            cellViewModels.removeAll()
        }
        
        factsService.facts(offset: pagingCursor.pageOffset, limit: pagingCursor.pageSize) { [weak self] (facts, error) in
            guard let strongSelf = self else { return }

            if let error = error {
                print("Error loading facts: " + error.localizedDescription)
                DispatchQueue.main.async {
                    self?.reloadCallback?(reload, nil, error)
                }
                return
            }

            strongSelf.incrementPage(fetchedItemsCount: facts?.count ?? 0)
            let insertIndexPaths = strongSelf.indexPathsForInsertedItems(itemsCountBeforeFetch: strongSelf.cellViewModels.count,
                                                                         fetchedItemsCount: facts?.count ?? 0)

            let newCellViewModels = facts?.map({ fact -> FactCellViewModelable in
                let cellViewModel = FactCellViewModel(fact: fact, resourceCache: strongSelf.resourceCache)
                cellViewModel.addObserverOnImageDidLoad(observer: strongSelf,
                                                        selector: #selector(strongSelf.remoteImageDidLoad(notification:)),
                                                        name: RemoteImageDisposer.ImageDidLoadNotification, object: nil)
                return cellViewModel
            })
            if let newCellViewModels = newCellViewModels {
                strongSelf.cellViewModels.append(contentsOf: newCellViewModels)
            }
            
            DispatchQueue.main.async {
                self?.reloadCallback?(reload, insertIndexPaths, error)
            }
        }        
    }
}


//MARK: - Notifications

extension FactsViewModel {
    
    @objc func remoteImageDidLoad(notification: Notification) {
        guard let image = notification.userInfo?[RemoteImageDisposer.ImageDidLoadNotificationImageKey] as? UIImage else { return }
        loadImageCallback?(image)
    }
}
