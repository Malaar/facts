//
//  FactDetailsViewModelable.swift
//  Facts
//
//  Created by Malaar on 6/11/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - FactDetailsViewModelable protocol

protocol FactDetailsViewModelable {
    
    typealias LoadImageCallback = (UIImage) -> Void
    
    var loadImageCallback: LoadImageCallback? { get set }

    func loadImage()
    func cancelLoadingImage()

    var factTitle: String? { get }
    var factDesctiption: String? { get }
}
