//
//  FactDetailsViewModel.swift
//  Facts
//
//  Created by Malaar on 6/11/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - FactDetailsViewModel

class FactDetailsViewModel: FactDetailsViewModelable {
    
    var factTitle: String? {
        return fact.title
    }

    var factDesctiption: String? {
        return fact.description
    }
    
    var loadImageCallback: LoadImageCallback? {
        get {
            return imageDisposer.loadImageCallback
        }
        set {
            imageDisposer.loadImageCallback = newValue
        }
    }

    private let fact: Factable
    private let imageDisposer: RemoteImageDisposable

    init(fact: Factable, resourceCache: ResourceCache) {
        self.fact = fact
        self.imageDisposer = RemoteImageDisposer(resourceCache: resourceCache)
    }
}


//MARK: - Image loading management

extension FactDetailsViewModel {
    
    func loadImage() {
        guard let imageUrl = fact.imageUrl else { return }
        imageDisposer.loadImage(imageUrl: imageUrl)
    }
    
    func cancelLoadingImage() {
        imageDisposer.cancelLoadingImage()
    }
}
