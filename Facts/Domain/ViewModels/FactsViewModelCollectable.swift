//
//  FactsViewModelCollectable.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - FactsViewModelCollectable protocol

protocol FactsViewModelCollectable: ViewModelPaginable {
 
    typealias LoadImageCallback = (UIImage) -> Void

    func itemsCount() -> Int
    func item(at indexPath: IndexPath) -> FactCellViewModelable?
    func fetchData(reload: Bool) -> Void
    
    var loadImageCallback: LoadImageCallback? { get set }
}
