//
//  FactCellViewModel.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import Mint


//MARK: - FactCellViewModel

class  FactCellViewModel: FactCellViewModelable {
    
    var factDetailsViewModel: FactDetailsViewModelable {
        return FactDetailsViewModel(fact: self.fact, resourceCache: resourceCache)
    }

    var title: String? { return fact.title }
    var imageSize: CGSize? {
        return image?.size
    }
    
    private(set) var image: UIImage?
    private let fact: Factable
    private let imageDisposer: RemoteImageDisposable
    private let resourceCache: ResourceCache

    init(fact: Factable, resourceCache: ResourceCache) {
        self.fact = fact
        self.resourceCache = resourceCache
        self.imageDisposer = RemoteImageDisposer(resourceCache: resourceCache)
        self.imageDisposer.loadImageCallback = { [weak self] image in
            self?.image = image
        }
    }
    
    deinit {
        cancelLoadingImage()
    }
}


//MARK: - Image loading management

extension FactCellViewModel {
    
    func loadImage() {
        guard let imageUrl = fact.imageUrl else { return }
        imageDisposer.loadImage(imageUrl: imageUrl)
    }
    
    func cancelLoadingImage() {
        imageDisposer.cancelLoadingImage()
    }

    func addObserverOnImageDidLoad(observer: Any, selector: Selector, name: NSNotification.Name?, object: Any?) {
        imageDisposer.imagesNotificationCenter.addObserver(observer, selector: selector, name: name, object: object)
    }
    
    func removeObserverOnImageDidLoad(observer: Any) {
        imageDisposer.imagesNotificationCenter.removeObserver(observer)
    }
}
