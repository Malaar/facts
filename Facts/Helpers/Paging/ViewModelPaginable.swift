//
//  ViewModelPaginable.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ViewModelPaginable protocol

protocol ViewModelPaginable: class {
    
    typealias ReloadCallback = (Bool, [IndexPath]?, Error?) -> Void
    
    var reloadCallback: ReloadCallback? { get set }
    var pagingCursor: PagingCursor { get set }
    var pagesFinished: Bool { get set }
}

extension ViewModelPaginable {
    
    func resetPage() {
        pagingCursor.pageOffset = 0
        pagesFinished = false
    }
    
    func incrementPage(fetchedItemsCount: Int) {
        pagingCursor.pageOffset += 1
        let currentPageSize = fetchedItemsCount
        if currentPageSize < pagingCursor.pageSize {
            pagesFinished = true
        }
    }    

    func indexPathsForInsertedItems(itemsCountBeforeFetch: Int, fetchedItemsCount: Int) -> [IndexPath] {
        let insertFromIndex = itemsCountBeforeFetch
        let insertToIndex = max(itemsCountBeforeFetch + fetchedItemsCount - 1, 0)
        let insertIndexPaths = Array(insertFromIndex...insertToIndex).map{ IndexPath(item: $0, section: 0) }
        return insertIndexPaths
    }
}
