//
//  PagingCursor.swift
//  Facts
//
//  Created by Malaar on 6/7/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Constants

fileprivate struct Constants {
    
    static let pageSize = 30
}


//MARK: - PagingCursor

struct PagingCursor {
    
    var pageOffset: Int
    let pageSize: Int
    
    init(pageOffset: Int = 0, pageSize: Int = Constants.pageSize) {
        self.pageOffset = pageOffset
        self.pageSize = pageSize
    }
}
