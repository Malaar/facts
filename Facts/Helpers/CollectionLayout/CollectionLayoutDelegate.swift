//
//  CollectionLayoutDelegate.swift
//  Facts
//
//  Created by Malaar on 6/12/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: Constants

fileprivate struct Constants {
    static let itemMargin: CGFloat = 10.0
    static let columnsNumber: Int = 2
}


//MARK: - CollectionLayoutDelegate

protocol CollectionLayoutDelegate: class {
    
    func collectionView(numberOfCollumnsForCollectionView: UICollectionView) -> Int
    func collectionView(itemMargin: UICollectionView) -> CGFloat
    func collectionView(_ collectionView: UICollectionView, heightForItemAtIndexPath indexPath: IndexPath, itemWidth: CGFloat) -> CGFloat
}

extension CollectionLayoutDelegate {
    
    func collectionView(numberOfCollumnsForCollectionView: UICollectionView) -> Int {
        return Constants.columnsNumber
    }
    
    func collectionView(itemMargin: UICollectionView) -> CGFloat {
        return Constants.itemMargin
    }
}
