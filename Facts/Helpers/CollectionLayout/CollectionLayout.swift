//
//  CollectionLayout.swift
//  Facts
//
//  Created by Malaar on 6/12/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - CollectionLayout

fileprivate struct Constants {
    
    static let itemMargin: CGFloat = 0.0
    static let columnsNumber: Int = 1
}


//MARK: - CollectionLayout

class CollectionLayout: UICollectionViewLayout {
    
    weak var delegate: CollectionLayoutDelegate?
    var shouldInvalidateAttributes: Bool = true
    
    private var itemMargin: CGFloat = Constants.itemMargin
    private var columnsNumber: Int = Constants.columnsNumber

    private var cache = [UICollectionViewLayoutAttributes]()
    private var contentHeight: CGFloat = 0.0
    private var contentWidth: CGFloat {
        guard let collectionView = self.collectionView else { return 0 }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
}


extension CollectionLayout {
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: self.contentWidth, height: self.contentHeight)
    }
    
    override func prepare() {
        guard let collectionView = self.collectionView, shouldInvalidateAttributes == true else { return }
        
        guard let delegate = self.delegate else {
            fatalError("Provide delegate for CollectionLayout!")
        }

        contentHeight = 0.0
        cache.removeAll()
        
        self.itemMargin = delegate.collectionView(itemMargin: collectionView)
        self.columnsNumber = delegate.collectionView(numberOfCollumnsForCollectionView: collectionView)
        
        let columnWidth = contentWidth / CGFloat(columnsNumber)
        let itemWidth = columnWidth - 2 * itemMargin
        var offsetX = (0 ..< columnsNumber).map { CGFloat($0) * columnWidth }
        var offsetY = [CGFloat](repeating: 0.0, count: columnsNumber)
        
        var column = 0
        for itemIndex in 0 ..< collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: itemIndex, section: 0)
            let itemHeight = delegate.collectionView(collectionView, heightForItemAtIndexPath: indexPath, itemWidth: itemWidth)
            let height = 2 * itemMargin + itemHeight
            let frame = CGRect(x: offsetX[column], y: offsetY[column], width: columnWidth, height: height)
            
            let insetFrame = frame.insetBy(dx: itemMargin, dy: itemMargin)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            
            contentHeight = max(contentHeight, frame.maxY)
            offsetY[column] = offsetY[column] + height
            
            column = column + 1 < columnsNumber ? column + 1 : 0
        }
        
        shouldInvalidateAttributes = false
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cache.filter { $0.frame.intersects(rect) }
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
    override func invalidateLayout() {
        shouldInvalidateAttributes = true
        super.invalidateLayout()
    }
}
