//
//  RemoteImageDisposable.swift
//  Facts
//
//  Created by Malaar on 6/11/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - RemoteImageDisposable protocol

protocol RemoteImageDisposable: class {
    
    typealias LoadImageCallback = (UIImage) -> Void
    
    var loadImageCallback: LoadImageCallback? { get set }
    var imagesNotificationCenter: NotificationCenter { get }

    func loadImage(imageUrl: URL)
    func cancelLoadingImage()
}
