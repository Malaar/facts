//
//  RemoteImageDisposer.swift
//  Facts
//
//  Created by Malaar on 6/11/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - RemoteImageDisposer

class RemoteImageDisposer: RemoteImageDisposable {

    static let ImageDidLoadNotification = Notification.Name(rawValue: "ImageDidLoadNotification")
    static let ImageDidLoadNotificationImageKey = "ImageDidLoadNotificationImageKey"
    static let ImageDidLoadNotificationImageUrlKey = "ImageDidLoadNotificationImageUrlKey"

    let imagesNotificationCenter = NotificationCenter()
    var loadImageCallback: LoadImageCallback?

    private let resourceCache: ResourceCache
    private var token: ResourceToken?

    init(resourceCache: ResourceCache) {
        self.resourceCache = resourceCache
    }
    
    deinit {
        cancelLoadingImage()
    }
}


//MARK: - Image loading management

extension RemoteImageDisposer {
    
    func loadImage(imageUrl: URL) {
        token?.cancel()
        token = resourceCache.resource(forURL: imageUrl) { [weak self] (resource, error) in
            self?.token = nil
            var image: UIImage?
            if let data = resource?.data {
                image = UIImage(data: data)
                if image == nil {
                    print("ERROR IMAGE CREATION: " + imageUrl.absoluteString )
                }
            } else {
                print("RESOURCE ERROR: Resource or data is nil!")
            }
            if let image = image {
                DispatchQueue.main.async { [weak self] in
                    self?.loadImageCallback?(image)
                    let userInfo = [RemoteImageDisposer.ImageDidLoadNotificationImageUrlKey: imageUrl,
                                    RemoteImageDisposer.ImageDidLoadNotificationImageKey: image] as [String : Any]
                    self?.imagesNotificationCenter.post(name: RemoteImageDisposer.ImageDidLoadNotification, object: self, userInfo: userInfo)
                }
            }
        }
    }
    
    func cancelLoadingImage() {
        token?.cancel()
        token = nil
    }
}
